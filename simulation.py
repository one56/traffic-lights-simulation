import random #generate random numbers_vehicles crossing signal
import time #to handle timer-related tasks
import threading #used for creating, controlling and managing threads in python
import pygame #graphics nd sounds

#Default values of signal timer in seconds
default_green = {0:20, 1:20, 2:20, 3:20}
default_red = 75
default_yellow = 5

signals = []
no_of_signals = 4
current_green = 0 #Indicates which signal is green currently
next_green = (current_green+1)%no_of_signals
current_yellow = 0 #Indicates whether yellow signal is on or off

speeds = {'car':2.25, 'bus':1.8, 'truck':1.8, 'bike':2.5} #Average speed of the vehicles

# Coordinates of vehicles’ start
x = {'right':[0,0,0], 'down':[755,727,697], 'left':[1400,1400,1400], 'up':[602,627,657]}
y = {'right':[348,370,398], 'down':[0,0,0], 'left':[498,466,436], 'up':[800,800,800]}

vehicles = {'right': {0:[], 1:[], 2:[], 3:[], 'crossed':0}, 'down': {0:[], 1:[], 2:[], 3:[], 'crossed':0}, 'left': {0:[], 1:[], 2:[], 3:[], 'crossed':0}, 'up': {0:[], 1:[], 2:[], 3:[], 'crossed':0}}
vehicleTypes = {0:'car', 1:'bus', 2:'truck', 3:'bike'}
directionNumbers = {0:'right', 1:'down', 2:'left', 3:'up'}

# Coordinates of signal image, timer, and vehicle count
signalCoordinates = [(530,230),(810,230),(810,570),(530,570)]    
signalTimerCoordinates = [(530,210),(810,210),(810,550),(530,550)]

# Coordinates of stop lines
stopLines = {'right': 590, 'down': 330, 'left': 800, 'up': 535}
defaultStop = {'right': 580, 'down': 320, 'left': 810, 'up': 545}

# Gap between vehicles
stoppingGap = 15    # stopping gap
movingGap = 15   # moving gap

pygame.init()
simulation = pygame.sprite.Group()  #this the group which holds objects

class TrafficSignal:
    def _init_(self, red, yellow, green):
        self.red = red
        self.yellow = yellow
        self.green = green
        self.signalText = ""


class Vehicle(pygame.sprite.Sprite):    #base class for visible object
    def _init_(self, lane, vehicleClass, direction_number, direction):
        pygame.sprite.Sprite._init_(self)
        self.lane = lane
        self.vehicleClass = vehicleClass
        self.speed = speeds[vehicleClass]
        self.direction_number = direction_number
        self.direction = direction
        self.x = x[direction][lane]
        self.y = y[direction][lane]
        self.crossed = 0
        self.index = len(vehicles[direction][lane]) - 1

        if (len(vehicles[direction][lane]) > 1 and vehicles[direction][lane][self.index - 1].crossed == 0):
            if (direction == 'right'):
                self.stop = vehicles[direction][lane][self.index - 1].stop - vehicles[direction][lane][self.index - 1].image.get_rect().width 
                - stoppingGap 
            elif (direction == 'left'):
                self.stop = vehicles[direction][lane][self.index - 1].stop + vehicles[direction][lane][self.index - 1].image.get_rect().width
                + stoppingGap
            elif (direction == 'down'):
                self.stop = vehicles[direction][lane][self.index - 1].stop - vehicles[direction][lane][self.index - 1].image.get_rect().height
                - stoppingGap
            elif (direction == 'up'):
                self.stop = vehicles[direction][lane][self.index - 1].stop + vehicles[direction][lane][self.index - 1].image.get_rect().height
                + stoppingGap
        else:
            self.stop = defaultStop[direction]

        # set new starting and stopping coordinate
        if (direction == 'right'):
            temp = self.image.get_rect().width + stoppingGap
            x[direction][lane] -= temp
        elif (direction == 'left'):
            temp = self.image.get_rect().width + stoppingGap
            x[direction][lane] += temp
        elif (direction == 'down'):
            temp = self.image.get_rect().height + stoppingGap
            y[direction][lane] -= temp
        elif (direction == 'up'):
            temp = self.image.get_rect().height + stoppingGap
            y[direction][lane] += temp
        simulation.add(self)

    def render(self, screen):
        screen.blit(self.image, (self.x, self.y))

        def move(self):
            if (self.direction == 'right'):
                if (self.crossed == 0 and self.x + self.image.get_rect().width > stopLines[self.direction]): # if the vehicle has crossed stop line or not
                    self.crossed = 1
                if ((self.x + self.image.get_rect().width <= self.stop or self.crossed == 1 or (currentGreen == 0 and currentYellow == 0))
                        and (self.index == 0 or self.x + self.image.get_rect().width 
                        < (vehicles[self.direction][self.lane][self.index - 1].x - movingGap))):
                #(if the vehicle has not reached its stop coordinate or has crossed stop line or has green signal) and (it is either first vehicle in that lane or it is has enough gap to the next vehicle in that lane)        
                    self.x += self.speed  #move the vehicle
            elif (self.direction == 'down'):
                if (self.crossed == 0 and self.y + self.image.get_rect().height > stopLines[self.direction]):
                    self.crossed = 1
                if ((self.y + self.image.get_rect().height <= self.stop or self.crossed == 1 or (currentGreen == 1 and currentYellow == 0))
                        and (self.index == 0 or self.y + self.image.get_rect().height
                             < (vehicles[self.direction][self.lane][self.index - 1].y - movingGap))):
                    self.y += self.speed
            elif (self.direction == 'left'):
                if (self.crossed == 0 and self.x < stopLines[self.direction]):
                    self.crossed = 1
                if ((self.x >= self.stop or self.crossed == 1 and (self.index == 0 or self.x)>
                 (vehicles[self.direction][self.lane][self.index - 1].x+ vehicles[self.direction][self.lane][self.index - 1].image.get_rect().width  + movingGap))):
                    self.x -= self.speed
            elif (self.direction == 'up'):
                if (self.crossed == 0 and self.y < stopLines[self.direction]):
                    self.crossed = 1
                if ((self.y >= self.stop or self.crossed == 1 or (currentGreen == 3 and currentYellow == 0)) and (self.index == 0 or self.y
                             > (vehicles[self.direction][self.lane][self.index - 1].y
                                + vehicles[self.direction][self.lane][self.index - 1].image.get_rect().height + movingGap))):
                    self.y -= self.speed
#Initialization of signals with default values                    
def initialize():
    ts1 = TrafficSignal(0, defaultYellow, defaultGreen[0])
    signals.append(ts1)
    ts2 = TrafficSignal(ts1.yellow+ts1.green, defaultYellow, defaultGreen[1])
    signals.append(ts2)
    ts3 = TrafficSignal(defaultRed, defaultYellow, defaultGreen[2])
    signals.append(ts3)
    ts4 = TrafficSignal(defaultRed, defaultYellow, defaultGreen[3])
    signals.append(ts4)
